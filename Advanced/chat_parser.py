import re
import nltk
import datetime
from time import strftime
import db_util

#Fixed Date (September 15th)
fixed_date = datetime.datetime.strptime('2017-09-15', "%Y-%m-%d")

#Get all the stations and lines from the database
stations = db_util.get_all_stations()
lines = db_util.get_all_lines()

#Get the word preceding the target word or words
def get_preceding_word(user_input, target):
    if target in user_input:
        #Split string based on target string
        split_input = user_input.lower().split(target.lower())
        #Split the preceding half string into words
        split_input_words = split_input[0].split(' ')
        #Clean up any empty strings
        split_input_words = [word for word in split_input_words if word]
        #Return the last word in the preceding half
        return split_input_words[-1:]

#All the possible reflections for dates
reflections = { 
        "first"         : "1st",
        "second"        : "2nd",
        "third"         : "3rd",
        "forth"         : "4th",
        "fifth"         : "5th",
        "sixth"         : "6th",
        "seventh"       : "7th",
        "eighth"        : "8th",
        "ninth"         : "9th",
        "tenth"         : "10th",
        "eleventh"      : "11th",
        "twelveth"      : "12th",
        "thirteenth"    : "13th",
        "fourteenth"    : "14th",
        "fifthteenth"   : "15th",
        "sixteenth"     : "16th",
        "seventeenth"   : "17th",
        "eightteenth"   : "18th",
        "nineteenth"    : "19th",
        "twentieth"     : "20th",
        "twenty-first"  : "21st",
        "twenty-second" : "22nd",
        "twenty-third"  : "23rd",
        "twenty-forth"  : "24th",
        "twenty-fifth"  : "25th",
        "twenty-sixth"  : "26th",
        "twenty-seventh": "27th",
        "twenty-eight"  : "28th",
        "twenty-ninth"  : "29th",
        "thirty"        : "30th",
        "thirty-first"  : "31th"
}

def edit_distance(str1, str2):
    
    #Empty case
    if len(str1) == 0:
        return len(str2)
    elif len(str2) == 0:
        return len(str1)
    
    prev_row = range(len(str1) + 1)
    
    for idx2, char2 in enumerate(str2):
        cur_row = [idx2 + 1]
        for idx1, char1 in enumerate(str1):
            if char1 == char2:
                cur_row += [prev_row[idx1]]
            else:
                cur_row += [1 + min(prev_row[idx1], prev_row[idx1 + 1], cur_row[-1])]
        prev_row = cur_row
    return prev_row[-1]


def spell_correct(data):

    #Constuct a list containing all keywords (Some shorter keywords are intentionally not included)
    general = ["timetable", "trackwork", "earlier", "later"]
    days = ["monday", "tuesday", "wednesday", "thursday", "friday", "saturday", "sunday"]
    keywords = stations + lines + general + days
    
    for key, value in reflections.items():
        keywords += [key]
    
    #Split keywords that are multi-worded in single words
    keywords = [word.split(" ") for word in keywords]
    #Unpack split word lists and flatten into single list 
    keywords = [item for sublist in keywords for item in sublist]
    
    # Split into words
    data = [w.lower() for w in nltk.word_tokenize(data)]
    
    # Enumerate through replacing any possible spelling errors with potential
    # keywords
    for idx, word in enumerate(data):
        if word not in keywords:
            for keyword in keywords:
                    distance = edit_distance(word.lower(), keyword.lower())
                    if (distance <= 1):
                        data[idx] = keyword
    
    return " ".join(data)
           
              
#Replace words with there reflections
def reflect(user_input):
    words = [w.lower() for w in nltk.word_tokenize(user_input)]
    for index, word in enumerate(words):
         if word in reflections:
            words[index] = reflections[word]       
    return words

#Seperate day parsing function due to complexity
def parse_day(user_input):

    words = reflect(user_input)
    date = None

    days = { "monday" : 0, "tuesday" : 1, "wednesday" : 2, "thursday" : 3, 
    "friday" : 4, "saturday" : 5, "sunday" : 6}
    
    #Convert a reference of a day to the next specific date that day occurs on
    for word in words:
        if word in days:
            days_ahead = days[word] - fixed_date.weekday()
            if days_ahead <= 0: 
                days_ahead += 7
            date =  fixed_date + datetime.timedelta(days_ahead)
    
    #Convert a date number e.g 15th to the actual date
    for word in words:
        if re.match(r'(\d+)(?:st|nd|rd|th)', word):
            num = re.findall(r'(\d+)(?:st|nd|rd|th)', word)[0]
            date = datetime.datetime.now()
            #Fixed month of sept
            newdate = date.replace(hour=00, minute=00, day=int(num), month=9)
            date = newdate

    if date:
        return strftime("%Y-%m-%d", date.timetuple())
    
    return None

#Parse input for any data
def parse_input(data):
    
    #Empty user input exit instantly
    if data == "":
        return
    
    #Run the spell correction
    data = spell_correct(data)
    
    parsed_input = dict()
    
    # Convert all words to lower case and go through them
    for word in [w.lower() for w in nltk.word_tokenize(data)]:
        
        # Extract any times in the HH:MM format
        if re.match(r'(\d{1,2}:\d{1,2})', word):
            
            #Remove am/pm and convert to 24 hour
            if "am" in word:
                word = word.replace("am","")
                split_word = word.split(":")
                if (int(split_word[0]) == 12):
                    word = "00:" + str(split_word[1])  
            elif "pm" in word:
                word = word.replace("pm","")
                split_word = word.split(":")
                if (int(split_word[0]) != 12):
                    word = str(int(split_word[0]) + 12) + ":" + str(split_word[1])
                
            #Check time is within valid range
            split_word = word.split(":")
            if (int(split_word[0]) < 24) and (int(split_word[1]) < 60): 
                parsed_input["time"] = word
        
        # Extract the mode (Either timetable or trackwork)
        if word == "timetable" or word == "trackwork":
            parsed_input["mode"] = word

        if word == "later" or word == "earlier":
            parsed_input["later_earlier"] = word
            
        if word == "yes" or word == "no":
            parsed_input["yes_no"] = word
    
    #Check for no for later_earlier       
    if "yes_no" in parsed_input:
        if parsed_input["yes_no"] == "no":
            parsed_input["later_earlier"] = "neither"
    
    # Extract any train lines from the data
    for line in lines:
        if line.lower() in data.lower():
            parsed_input["line"] = line
                        
    # Extract any train stations from the data
    for station in stations:
        if station.lower() in data.lower():
            parsed_input["station"] = station
                     
    # Extract any references to days or dates
    parsed_day = parse_day(data)
    if parsed_day:
        parsed_input["day"] = parsed_day
    
    # Extract any trips (Two stations mentioned in the same input) (For advanced task)
    train_trip = ["",""]
    for station in stations:
        if station.lower() in data.lower():
            # Use from/to to determine which is the source and which is the
            # destination, otherwise just use the ordering of whichever occurs
            # first
            if get_preceding_word(data.lower(), station.lower()) == ["from"]:
                if train_trip[0]:
                    train_trip[1] = train_trip[0]
                train_trip[0] = station
            elif get_preceding_word(data.lower(), station.lower()) == ["to"]:
                train_trip[1] = station            
            elif not train_trip[0]:
                train_trip[0] = station
            elif not train_trip[1]:
                train_trip[1] = station
    
    if train_trip[0] and train_trip[1]:
         parsed_input["trip"] = train_trip
    
    #Combine day and time into single day_time (For advanced task)
    if "day" in parsed_input and "time" in parsed_input:
        parsed_input["day_time"] = [parsed_input["day"], parsed_input["time"]]
    
    # Return a dictionary containing the parsed data
    return parsed_input
