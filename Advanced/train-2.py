import chat
import db_util
import datetime
import sys

# Python files
# Conversation Structure/logic  	  train-1.py
# Dialog functions                  chat.py 
# All parsing functions 		     chat_parser.py
# All database functions  		     db_util.py
# Database builder				     train-db.py

# Every Dialog has the following structure
#   [0] The intial inquiry message ("What is the time")
#   [1] The invalid input message  ("That is not correct, What is the time?")
#   [2] The help message           ("Input the time in 24 hour format hh:mm")
#   [3] The expected input type    (time, station, day ETC)
#   [4] The parsed users reponse   ({"time" : "11:30"}, Initially empty)

dialogues = {

    "timetable_or_trackwork"   : ["\nWould you like timetable information or information on trackwork? \n\n",
                                  "\nI'm not sure if that is timetable or trackwork. (If you are not sure how to answer reply 'help')",
                                  "\nI'm looking for the word 'timetable' or 'trackwork' indicating your choice.", 
                                  "mode", ""],

    "timetable_trip"           : ["\nSure, What station will you be departing from and what is your destination station?\n\n",
                                  "\nI'm not sure what stations you wish to depart from and travel to are.(If you are not sure how to answer reply 'help')",
                                  "\nI'm looking for two stations, indiciating your departing and destatinion stations E.G 'Epping to Central'",  
                                  "trip", ""],

    "timetable_day_time"       : ["\nAlright, from {d[timetable_trip][4][0]} to {d[timetable_trip][4][1]} and on what day and at what time will you be travelling?\n\n",
                                  "\nI'm not sure what day and what time you wish to travel. (If you are not sure how to answer reply 'help')",
                                  "\nI'm looking for a day (or date) and time indicating when you will be travelling E.G 'Monday at 10:00'", 
                                  "day_time", ""],    

    "timetable_earlier_later"  : ["\nLet me see - I have a train leaving {o[trip][0]} at {o[trip][1]} and arriving at {o[trip][2]} at {o[trip][3]}. Would you like an earlier or later train?\n\n",
                                  "\nI'm not sure if you want an earlier or later train. (If you are not sure how to answer reply 'help')",
                                  "\nI'm looking for a message containing 'earlier', 'later' or 'no'", 
                                  "later_earlier", ""],   

    "timetable_earlier"        : ["\nLet me check - I have a train leaving {o[trip][0]} at {o[trip][1]} and arriving at {o[trip][2]} at {o[trip][3]}. Would you like an earlier train?\n\n",
                                  "\nI'm not sure if that was a yes or no. (If you are not sure how to answer reply 'help')",
                                  "\nI'm looking for a 'yes' or 'no' answer to the question.", 
                                  "yes_no", ""],   

    "timetable_later"          : ["\nLet me check - I have a train leaving {o[trip][0]} at {o[trip][1]} and arriving at {o[trip][2]} at {o[trip][3]}. Would you like a later train?\n\n",
								  "\nI'm not sure if that was a yes or no. (If you are not sure how to answer reply 'help')",
								  "\nI'm looking for a 'yes' or 'no' answer to the question.", 
								  "yes_no", ""],     

    "trackwork_line"           : ["\nOn what train line are you planning to travel on? \n\n",
							 	  "\nI'm not sure what line that is. (If you are not sure how to answer reply 'help')",
								  "\nI'm looking for the name of the train line you are travelling on (E.G 'Northern Line')", 
								  "line", ""],

    "trackwork_day_time"       : ["\nOn what day and at what time will you be travelling on the {d[trackwork_line][4]}? \n\n",
								  "\nI'm not sure what day and what time that is (If you are not sure how to answer reply 'help')",
								  "\nI'm looking for a day (or date) and time indicating when you will be travelling E.G 'Sixteenth at 10:00'",
								  "day_time", ""]           
}


# All the simple non-dialogue messages used throughout
strings = { 
    "welcome"      : ("\nWelcome to Sydney Train's text-based rail information service. This service will help you find convenient ways to travel by asking you a number of questions. If you are not sure about how to answer a question, simply type 'help'. You can 'quit' the conversation anytime."),
    "end"          : ("\nOkay, thank you for travelling with Sydney Train - cost effective, reliable and convenient."),           
    "no_train"     : ("\nThere is no train available for the route, time and day you have specified please choose another route, time and day."),            
    "trackwork"    : ("\nThere is trackwork on the {0} on {1} to {2}; {3}"),   
    "trackwork_2"  : ("\nTRACKWORK NOTICE: The trip currently selected ({0} -> {1} on {2}) is on the {3} which has trackwork from {4} to {5} ({6}) that would interfere with your trip."),  
    "to_late"      : ("\nThere is no later times for this train. Please select another trip, day, or time."),   
    "to_early"     : ("\nThere is no earlier times for this train. Please select another trip, day, or time."),  
    "no_trackwork" : ("\nThere is no trackwork on {0}.")
}

# Helper Functions

# Check if a train has been found in the specified time, route
def check_train_found(train_idx, train_len):
   if (train_idx < 0) or (train_idx >= train_len):        
       chat.clear_answer("timetable_trip", dialogues)
       chat.clear_answer("timetable_day_time", dialogues)
       chat.clear_answer("timetable_earlier_later", dialogues)
       chat.clear_answer("timetable_earlier", dialogues)
       chat.clear_answer("timetable_later", dialogues)
       return False
   return True

#Get the line for a particular station and check for trackwork at the time
def check_trackwork_advanced(trip, day, time):

    daytime = day + ' ' + time
    
    #Format
    daytime_obj = datetime.datetime.strptime(daytime, "%Y-%m-%d %H:%M")
    formatted_daytime = daytime_obj.strftime("%Y-%m-%d %H:%M")
    
    line = db_util.get_line_for_station(trip[0])
    work = db_util.get_trackwork_advanced(line, formatted_daytime)
    
    for r in work:
        #Format and print
        selected = datetime.datetime.strptime(daytime, "%Y-%m-%d %H:%M")
        start = datetime.datetime.strptime(r[1], "%Y-%m-%d %H:%M")
        end = datetime.datetime.strptime(r[2], "%Y-%m-%d %H:%M")                     
        print(strings["trackwork_2"].format(trip[0], trip[1], 
              selected.strftime("%A %d %B %H:%M"),
              r[0],
              start.strftime("%A %d %B %H:%M"),
              end.strftime("%A %d %B %H:%M"),
              r[3]))

def converse():
    
    has_trip_been_found = False
    train_trip_index = 0
    
    # Move through all dialogue options in the required order and collect the
    # users answers to them
    while True:
        
        if not chat.answered("timetable_or_trackwork", dialogues):
            
            chat.ask_dialogue("timetable_or_trackwork", dialogues)
            
        elif chat.get_answer("timetable_or_trackwork", dialogues) == "timetable":
            
            if not chat.answered("timetable_trip", dialogues): 
                
                chat.ask_dialogue("timetable_trip", dialogues)
                
            elif not chat.answered("timetable_day_time", dialogues): 
                
                chat.ask_dialogue("timetable_day_time", dialogues)
                
            else:
                
                if not has_trip_been_found:
  
                    train_trip_index = -1
                                        
                    # Get all the trains between the destination and source along with their times
                    source = chat.get_answer("timetable_trip", dialogues)[0]
                    dest = chat.get_answer("timetable_trip", dialogues)[1]
                    
                    # Get the day type (Weekend/weekday)
                    day = chat.get_answer("timetable_day_time", dialogues)[0]
                    day_obj = datetime.datetime.strptime(day, "%Y-%m-%d")
                    
                    if (day_obj.weekday() < 5):
                        trains = db_util.get_trains(source, dest, "WD")
                    else:
                        trains = db_util.get_trains(source, dest, "WE")
                          
                    # Get the time inputted by the user (Convert it to time obj)
                    time = chat.get_answer("timetable_day_time", dialogues)[1]
                    time = datetime.datetime.strptime(time,'%H:%M')

                    # Loop through all the train depart times, and find the closest to the user input
                    for idx, (_, d_time, _, _) in enumerate(trains):
                        if (time <= datetime.datetime.strptime(d_time,'%H:%M')):
                            train_trip_index = idx
                            has_trip_been_found = True
                            break
                       
                    # If no train time has been found the time was to late, ask the user for another time
                    if not check_train_found(train_trip_index, len(trains)):
                        print(strings["no_train"])
                        
                else:
    


                                      
                   if not chat.get_answer("timetable_earlier_later", dialogues):
                       
                        check_trackwork_advanced(
                             chat.get_answer("timetable_trip", dialogues),
                             chat.get_answer("timetable_day_time", dialogues)[0],
                             trains[train_trip_index][1]
                        )
                                          
                        #Ask the user if they want earlier or later times   
                        trip_dict = {"trip" : trains[train_trip_index]}
                        chat.ask_dialogue("timetable_earlier_later", dialogues, trip_dict)
                        
                   elif chat.get_answer("timetable_earlier_later", dialogues) == "later":
                       
                       # Check if the user wants an later time or not
                       if chat.get_answer("timetable_later", dialogues) == "no":
                           sys.exit()
                       else:
                           train_trip_index += 1
                           

                           if check_train_found(train_trip_index, len(trains)):
                               
                               check_trackwork_advanced(
                                       chat.get_answer("timetable_trip", dialogues),
                                       chat.get_answer("timetable_day_time", dialogues)[0],
                                       trains[train_trip_index][1]
                               )
                               
                               trip_dict = {"trip" : trains[train_trip_index]}    
                               chat.ask_dialogue("timetable_later", dialogues , trip_dict)
                           else:
                               print(strings["to_late"])
                               has_trip_been_found = False
                           
                   elif chat.get_answer("timetable_earlier_later", dialogues) == "earlier": 
                       
                       # Check if the user wants an earlier time or not
                       if chat.get_answer("timetable_earlier", dialogues) == "no":
                           sys.exit()
                       else:
                           train_trip_index -= 1

                           if check_train_found(train_trip_index, len(trains)):
                               
                               check_trackwork_advanced(
                                       chat.get_answer("timetable_trip", dialogues),
                                       chat.get_answer("timetable_day_time", dialogues)[0],
                                       trains[train_trip_index][1]
                               )
                               
                               trip_dict = {"trip" : trains[train_trip_index]}    
                               chat.ask_dialogue("timetable_earlier", dialogues , trip_dict)
                           else:
                               print(strings["to_early"])
                               has_trip_been_found = False
                               
                           
                   elif chat.get_answer("timetable_earlier_later", dialogues) == "neither":
                       sys.exit()
                                      
                
        elif chat.get_answer("timetable_or_trackwork", dialogues) == "trackwork":
            
            if not chat.answered("trackwork_line", dialogues):  
                
                chat.ask_dialogue("trackwork_line", dialogues)
                
            elif not chat.answered("trackwork_day_time", dialogues):
                
                chat.ask_dialogue("trackwork_day_time", dialogues)  
                
            else:
                
                line =  chat.get_answer("trackwork_line", dialogues)
                
                #Combine into correct format for parsing
                daytime = chat.get_answer("trackwork_day_time", dialogues)[0]
                daytime += ' ' + chat.get_answer("trackwork_day_time", dialogues)[1]
                
                #Format
                daytime_obj = datetime.datetime.strptime(daytime, "%Y-%m-%d %H:%M")
                formatted_daytime = daytime_obj.strftime("%Y-%m-%d %H:%M")
                
                # Get trackwork for the users input
                trackwork = db_util.get_trackwork_advanced(line, formatted_daytime)

                # Print the trackwork information
                if not trackwork:
                    print(strings["no_trackwork"].format(daytime_obj.strftime("%A, %d %B at %H:%M")))
                else:
                    for r in trackwork: 
                        #Format and print the trackwork
                        start = datetime.datetime.strptime(r[1], "%Y-%m-%d %H:%M")
                        end = datetime.datetime.strptime(r[2], "%Y-%m-%d %H:%M")
                        print(strings["trackwork"].format(r[0],
                              start.strftime("%A %d %B %H:%M"),
                              end.strftime("%A %d %B %H:%M"),
                              r[3]))
                    
                # Clear previous dialogue responses
                chat.clear_answer("timetable_or_trackwork", dialogues)
                chat.clear_answer("trackwork_line", dialogues)
                chat.clear_answer("trackwork_day_time", dialogues)

def main():
    
    try:
        print(strings["welcome"])
        converse()
    except SystemExit:
        print(strings["end"])
        #Ask for input to pause before exiting
        input()
        return

if __name__ == "__main__":
    main()