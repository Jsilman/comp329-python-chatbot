import chat
import db_util
import datetime
import sys

# Python files
# Conversation Structure/logic  	  train-1.py
# Dialog functions                  chat.py 
# All parsing functions 		     chat_parser.py
# All database functions  		     db_util.py
# Database builder				     train-db.py

# Every Dialog has the following structure
#   [0] The intial inquiry message ("What is the time")
#   [1] The invalid input message  ("That is not correct, What is the time?")
#   [2] The help message           ("Input the time in 24 hour format hh:mm")
#   [3] The expected input type    (time, station, day ETC)
#   [4] The parsed users reponse   ({"time" : "11:30"}, Initially empty)

dialogues = {

    "timetable_or_trackwork"   : ["\nWould you like timetable information or information on trackwork? \n\n",
                                  "\nI'm not sure if that is timetable or trackwork. (If you are not sure how to answer reply 'help')",
                                  "\nI'm looking for the word 'timetable' or 'trackwork' indicating your choice.", 
                                  "mode", ""],

    "timetable_source"         : ["\nSure. What station would you like to leave from?\n\n",
                                  "\nI'm not sure what station you wish to leave from (If you are not sure how to answer reply 'help')",
                                  "\nI'm looking for a station, indiciating where your departing from E.G 'Epping'",  
                                  "station", ""],
								  
    "timetable_destination"     : ["\nOkay, {d[timetable_source][4]}. And where are you travelling to?\n\n",
                                  "\nI'm not sure what station you wish to travel to (If you are not sure how to answer reply 'help')",
                                  "\nI'm looking for a station, indiciating where your travelling to E.G 'Central'",  
                                  "station", ""],							  

    "timetable_day"            : ["\nOkay, to {d[timetable_destination][4]} and do you want to travel on a weekday or on a weekend?\n\n",
                                  "\nI'm not sure what day and what time you wish to travel. (If you are not sure how to answer reply 'help')",
                                  "\nI'm looking for a day indicating when you will be travelling E.G 'Monday'", 
                                  "day", ""],    
	
    "timetable_time"           : ["\nTravelling on {o[day]}, and what time would you like to depart from {d[timetable_source][4]}?\n\n",
                                  "\nI'm not sure what day and what time you wish to travel. (If you are not sure how to answer reply 'help')",
                                  "\nI'm looking for a time in 24 hour HH:MM format indicating when you will be travelling E.G '10:00'", 
                                  "time", ""],    	

    "timetable_earlier_later"  : ["\nLet me see - I have a train leaving {o[trip][0]} at {o[trip][1]} and arriving at {o[trip][2]} at {o[trip][3]}. Would you like an earlier or later train?\n\n",
                                  "\nI'm not sure if you want an earlier or later train. (If you are not sure how to answer reply 'help')",
                                  "\nI'm looking for a message containing 'earlier', 'later' or 'no'", 
                                  "later_earlier", ""],   

    "timetable_earlier"        : ["\nLet me check - I have a train leaving {o[trip][0]} at {o[trip][1]} and arriving at {o[trip][2]} at {o[trip][3]}. Would you like an earlier train?\n\n",
                                  "\nI'm not sure if that was a yes or no. (If you are not sure how to answer reply 'help')",
                                  "\nI'm looking for a 'yes' or 'no' answer to the question.", 
                                  "yes_no", ""],   

    "timetable_later"          : ["\nLet me check - I have a train leaving {o[trip][0]} at {o[trip][1]} and arriving at {o[trip][2]} at {o[trip][3]}. Would you like a later train?\n\n",
								  "\nI'm not sure if that was a yes or no. (If you are not sure how to answer reply 'help')",
								  "\nI'm looking for a 'yes' or 'no' answer to the question.", 
								  "yes_no", ""],     

    "trackwork_line"           : ["\nOn what line are you planning to travel on? \n\n",
							 	  "\nI'm not sure what line that is. (If you are not sure how to answer reply 'help')",
								  "\nI'm looking for the name of the train line you are travelling on (E.G 'Northern Line')", 
								  "line", ""],

    "trackwork_day"            : ["\nOn what day will you be travelling? \n\n",
								  "\nI'm not sure what day and what time that is (If you are not sure how to answer reply 'help')",
								  "\nI'm looking for a day indicating what day you want to check for trackwork on E.G 'Sixteenth'",
								  "day", ""],  

    "trackwork_time"           : ["\nAt what time? \n\n",
								  "\nI'm not sure what time that is (If you are not sure how to answer reply 'help')",
								  "\nI'm looking for a time in 24 hour HH:MM format indicating what time you want to check for trackwork at E.G '10:00'",
								  "time", ""]   								  
}


# All the simple non-dialogue messages used throughout
strings = { 
    "welcome"      : ("\nWelcome to Sydney Train's text-based rail information service. This service will help you find convenient ways to travel by asking you a number of questions. If you are not sure about how to answer a question, simply type 'help'. You can 'quit' the conversation anytime."),
    "end"          : ("\nOkay, thank you for travelling with Sydney Train - cost effective, reliable and convenient."),           
    "no_train"     : ("\nThere is no train available for the route, time and day you have specified, please choose another route, time and day."),            
    "trackwork"    : ("\nThere is trackwork on the {0} on {1} to {2}; {3}"),   
    "trackwork_2"  : ("\nNOTICE: There is trackwork on the {0} on {1} to {2}; {3}"), 
    "to_late"      : ("\nThere is no later times for this train. Please select another trip, day, or time."),   
    "to_early"     : ("\nThere is no earlier times for this train. Please select another trip, day, or time."),  
    "no_trackwork" : ("\nThere is no trackwork on {0}.")
}

# Helper Functions

# Check if a train has been found in the specified time/trip otherwise reset
def check_train_found(train_idx, train_len):
   if (train_idx < 0) or (train_idx >= train_len):          
       chat.clear_answer("timetable_source", dialogues)
       chat.clear_answer("timetable_destination", dialogues)
       chat.clear_answer("timetable_day", dialogues)
       chat.clear_answer("timetable_time", dialogues)
       chat.clear_answer("timetable_earlier_later", dialogues)
       chat.clear_answer("timetable_earlier", dialogues)
       chat.clear_answer("timetable_later", dialogues)
       return False
   return True

def converse():
    
    has_trip_been_found = False
    train_trip_index = 0
    
    # Move through all dialogue options in the required order and collect the
    # users answers to them
    while True:
        
        if not chat.answered("timetable_or_trackwork", dialogues):
            
            chat.ask_dialogue("timetable_or_trackwork", dialogues)
            
        elif chat.get_answer("timetable_or_trackwork", dialogues) == "timetable":
            
            if not chat.answered("timetable_source", dialogues): 
                
                chat.ask_dialogue("timetable_source", dialogues)
                
            elif not chat.answered("timetable_destination", dialogues): 
                
                chat.ask_dialogue("timetable_destination", dialogues)
                
            elif not chat.answered("timetable_day", dialogues): 
                
                chat.ask_dialogue("timetable_day", dialogues)
            
            elif not chat.answered("timetable_time", dialogues): 
                
                #Get users input day and convert it to required format
                day = chat.get_answer("timetable_day", dialogues)
                day = datetime.datetime.strptime(day,'%Y-%m-%d')
                day_dict = {"day" : day.strftime("%A , %B %d")}
                
                chat.ask_dialogue("timetable_time", dialogues, day_dict)
                
            else:
                
                if not has_trip_been_found:
  
                    train_trip_index = -1
                                        
                    # Get all the trains between the destination and source along with their times
                    source = chat.get_answer("timetable_source", dialogues)
                    dest = chat.get_answer("timetable_destination", dialogues)
                    
                    # Get the day type (Weekend/weekday)
                    day = chat.get_answer("timetable_day", dialogues)
                    day_obj = datetime.datetime.strptime(day, "%Y-%m-%d")
                    
                    if (day_obj.weekday() < 5):
                        trains = db_util.get_trains(source, dest, "WD")
                    else:
                        trains = db_util.get_trains(source, dest, "WE")
                                                  
                    # Get the time inputted by the user (Convert it to time obj)
                    time = chat.get_answer("timetable_time", dialogues)
                    time = datetime.datetime.strptime(time,'%H:%M')

                    # Loop through all the train depart times, and find the closest to the user input
                    for idx, (_, d_time, _, _) in enumerate(trains):
                        if (time <= datetime.datetime.strptime(d_time,'%H:%M')):
                            train_trip_index = idx
                            has_trip_been_found = True
                            break
                       
                    # If no train time has been found the time was to late, ask the user for another time
                    if not check_train_found(train_trip_index, len(trains)):
                       print(strings["no_train"])
                    
                else:
   
                   if not chat.get_answer("timetable_earlier_later", dialogues):
                       
                        #Ask the user if they want earlier or later times   
                        trip_dict = {"trip" : trains[train_trip_index]}
                        chat.ask_dialogue("timetable_earlier_later", dialogues, trip_dict)
                        
                   elif chat.get_answer("timetable_earlier_later", dialogues) == "later":
                       
                       # Check if the user wants an later time or not
                       if chat.get_answer("timetable_later", dialogues) == "no":
                           sys.exit()
                       else:
                           #Get a later train (and check if it exists)
                           train_trip_index += 1
                           if check_train_found(train_trip_index, len(trains)):
                               trip_dict = {"trip" : trains[train_trip_index]}    
                               chat.ask_dialogue("timetable_later", dialogues , trip_dict)
                           else:
                               print(strings["to_late"])
                               has_trip_been_found = False
                           
                   elif chat.get_answer("timetable_earlier_later", dialogues) == "earlier": 
                       
                       # Check if the user wants an earlier time or not
                       if chat.get_answer("timetable_earlier", dialogues) == "no":
                           sys.exit()
                       else:
                           #Get an earlier train (and check if it exists)
                           train_trip_index -= 1
                           if check_train_found(train_trip_index, len(trains)):
                               trip_dict = {"trip" : trains[train_trip_index]}    
                               chat.ask_dialogue("timetable_earlier", dialogues , trip_dict)
                           else:
                               print(strings["to_early"])
                               has_trip_been_found = False
                               
                           
                   elif chat.get_answer("timetable_earlier_later", dialogues) == "neither":
                       sys.exit()
                                      
                
        elif chat.get_answer("timetable_or_trackwork", dialogues) == "trackwork":
            
            if not chat.answered("trackwork_line", dialogues):  
                
                chat.ask_dialogue("trackwork_line", dialogues)
                
            elif not chat.answered("trackwork_day", dialogues):
                
                chat.ask_dialogue("trackwork_day", dialogues)  
                
            elif not chat.answered("trackwork_time", dialogues):
                
                chat.ask_dialogue("trackwork_time", dialogues) 
            else:
                
                line =  chat.get_answer("trackwork_line", dialogues)
                
                #Combine into correct format for sql
                daytime = chat.get_answer("trackwork_day", dialogues)
                daytime += ' ' + chat.get_answer("trackwork_time", dialogues)
                
                #Format
                daytime_obj = datetime.datetime.strptime(daytime, "%Y-%m-%d %H:%M")
                formatted_daytime = daytime_obj.strftime("%Y-%m-%d %H:%M")
                
                # Get trackwork for the users input
                trackwork = db_util.get_trackwork_advanced(line, formatted_daytime)

                # Print the trackwork information
                if not trackwork:
                    daytime_obj = datetime.datetime.strptime(daytime, "%Y-%m-%d %H:%M")
                    print(strings["no_trackwork"].format(daytime_obj.strftime("%A, %d %B at %H:%M")))
                else:
                    for r in trackwork: 
                        #Format and print the trackwork
                        start = datetime.datetime.strptime(r[1], "%Y-%m-%d %H:%M")
                        end = datetime.datetime.strptime(r[2], "%Y-%m-%d %H:%M")
                        print(strings["trackwork"].format(r[0],
                              start.strftime("%A %d %B %H:%M"),
                              end.strftime("%A %d %B %H:%M"),
                              r[3]))
                    
                # Clear previous dialogue responses
                chat.clear_answer("timetable_or_trackwork", dialogues)
                chat.clear_answer("trackwork_line", dialogues)
                chat.clear_answer("trackwork_time", dialogues)
                chat.clear_answer("trackwork_day", dialogues)

def main():
    try:
        print(strings["welcome"])
        converse()
    except SystemExit:
        print(strings["end"])
        #Ask for input to pause before exiting
        input()
        return

if __name__ == "__main__":
    main()