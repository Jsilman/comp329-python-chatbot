import sqlite3

conn = sqlite3.connect('train.db')
# ------------------------------------------------------------
# SQL Functions retreival functions
# ------------------------------------------------------------

def get_trains(source, destination, day):
   cursor = conn.execute("""
   SELECT '{0}', x.Departure_Time, '{1}',
          strftime('%H:%M',datetime(y.Departure_Time, '-1 minute'))  
   FROM   Schedule x, Schedule y
   WHERE  x.Station_ID = (SELECT x.Station_ID FROM Station x
                          WHERE  x.Station_Name = '{0}') 
          AND EXISTS (SELECT y.Station_ID
                      WHERE  y.Train_ID = x.Train_ID
                             AND y.Station_ID = (SELECT Station_ID FROM Station
                                                 WHERE  Station_Name = '{1}')
                      AND  x.Departure_Time < y.Departure_Time)
          AND (SELECT Part_Of_Week
               FROM   Train
               WHERE  Train_ID = x.Train_ID) = '{2}'
   ORDER BY
         ABS(strftime('%s', x.Departure_Time) - strftime('%s', '10:45')) ASC;
   """.format(source, destination, day))
   trains = [(r[0], r[1], r[2], r[3]) for r in cursor.fetchall()]
   trains = sorted(trains, key=lambda route: route[1])
   return trains
    

#Get the trackwork occuring
def get_trackwork():
    cursor = conn.execute("""
    SELECT Line_Name, Start_Date, End_Date, Message
        FROM  Trackwork x, Line y  
        WHERE x.Line_ID = y.LINE_ID AND Line_Name = 'Northern Line' AND
              (strftime( '%s', Start_Date ) <= strftime( '%s', '2017-09-16 10:00') AND
               strftime( '%s', End_Date )   >= strftime( '%s', '2017-09-16 10:00') )""")
    trackwork = [(r[0], r[1], r[2], r[3]) for r in cursor.fetchall()]
    return trackwork

def get_trackwork_advanced(line, day_time):
    cursor = conn.execute("""
    SELECT Line_Name, Start_Date, End_Date, Message
        FROM  Trackwork x, Line y  
        WHERE x.Line_ID = y.LINE_ID AND Line_Name = '{0}' AND
              (strftime( '%s', Start_Date ) <= strftime( '%s', '{1}') AND
               strftime( '%s', End_Date )   >= strftime( '%s', '{1}') )""".format(line, day_time))
    trackwork = [(r[0], r[1], r[2], r[3]) for r in cursor.fetchall()]
    return trackwork

def get_line_for_station(station):
    cursor = conn.execute("""
                             SELECT Line_Name 
                             FROM Line 
                             WHERE Line_ID = (SELECT sl.Line_ID 
                                              FROM Station_Line sl 
                                              WHERE sl.Station_ID = (SELECT s.Station_ID 
                                                                     FROM Station s  
                                                                     WHERE s.Station_Name = '{0}'))     
                             """.format(station))
    line = [r[0] for r in cursor.fetchall()]
    return line[0]

def get_all_stations():
    cursor = conn.execute("SELECT Station_Name FROM Station")
    stations = [r[0] for r in cursor.fetchall()]
    return stations

def get_all_lines():
    cursor = conn.execute("SELECT Line_Name FROM Line")
    lines = [r[0] for r in cursor.fetchall()]
    return lines
      