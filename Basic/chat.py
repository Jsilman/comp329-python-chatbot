import chat_parser
import sys

# Every Dialog has the following structure
#   [0] The intial inquiry message ("What is the time")
#   [1] The invalid input message  ("That is not correct, I am expecting a time")
#   [2] The help message           ("Input the time in 24 hour format hh:mm")
#   [3] The expected input type    (time, station, day ETC)
#   [4] The parsed users reponse   ({"time" : "11:30"}, Initially empty)

def ask_dialogue(dialogue_key, dialogue_dict, optional_dict=None):

    #Keep track of data entered
    has_entered_valid_data = False
    
    #Keep asking until we recieve the expected data
    while has_entered_valid_data is False:
        
        user_input = input(dialogue_dict[dialogue_key][0].format(d=dialogue_dict, o=optional_dict))
        
        if user_input.lower() == "help":
            print(dialogue_dict[dialogue_key][2].format(d=dialogue_dict))
        elif user_input.lower() == "quit":
            sys.exit(1)
        else:
            #If general input is given parse it
            parsed_input = chat_parser.parse_input(user_input)
            if parsed_input:
                #If parsed input is returned (Dictionary) check that it contains the key we are
                #are expecting E.G "time" or "staion" indication that the users valid input has
                #been found.
                if dialogue_dict[dialogue_key][3] in parsed_input:
                    dialogue_dict[dialogue_key][4] = parsed_input[dialogue_dict[dialogue_key][3]]
                    has_entered_valid_data = True
                else:
                    # Notify user of invalid input
                    print(dialogue_dict[dialogue_key][1].format(d=dialogue_dict, o=optional_dict))
            else:
                # Notify user of invalid input
                print(dialogue_dict[dialogue_key][1].format(d=dialogue_dict, o=optional_dict))

#Check if a dialogue has been answer (If [4] is empty then it has not been)
def answered(dialogue_key, dialogue_dict):
    if not dialogue_dict[dialogue_key][4]:
        return False
    else: 
        return True

#Return the stored user answer 
def get_answer(dialogue_key, dialogue_dict):
    return dialogue_dict[dialogue_key][4]

#Clear the stored user answer
def clear_answer(dialogue_key, dialogue_dict):
    dialogue_dict[dialogue_key][4] = None
                 